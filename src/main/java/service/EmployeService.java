/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.mycompany.boutique.Employee;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author toma
 */
public class EmployeService {
    
    static List<Employee> liste;
    
    // Ajouter un employer
    public void ajouter(Employee employee) {
        liste.add(employee);
    }
    
    // Modifier un employee
    public void modifier(Employee e) {
        for(Employee employee : liste) {
            if(employee.getId().equals(e.getId())) {
                liste.set(liste.indexOf(employee), e);
                return;
            }
        }
    }
    
    // Trouver un employee
    public Employee trouver(Integer id) {
        Employee find = new Employee();
        for(Employee employee : liste) {
            if(employee.getId().equals(id)) {
                find = employee;
                break;
            }
        }
        return find;
    }
    
    // Supprimer un employee
    public void supprimer(Integer id) {
        for(Employee employee : liste) {
            if(employee.getId().equals(id)) {
                liste.remove(employee);
                return;
            }
        }
    }
    
    // Supprimer un employer donné
    public void supprimer(Employee employee) {
        liste.remove(employee);
    }
    
    // Lister les empliyees
    public List<Employee> lister() {
        return liste;
    }
    
    // Liste des employés à partir d'un nombre
    public List<Employee> lister(int debut, int nombre) {
        List<Employee> element = new LinkedList<>();
        
        for(int i = debut - 1; i < nombre; i++) {
            element.add(liste.get(i));
        }
        
        return element;
    }
}
