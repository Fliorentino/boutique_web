/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.mycompany.boutique.Client;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author toma
 */
public class ClientService {
    
    static List<Client> liste;
    
    // Ajouter un client
    public void ajouter(Client client) {
        liste.add(client);
    }
    
    // Modifier un client 
    public void modifier(Client e) {
        for(Client client : liste) {
            if(client.getId().equals(e.getId())) {
                liste.set(liste.indexOf(client), e);
                return;
            }
        }
    }
    
    // Trouver un client
    public Client trouver(Integer id) {
        Client find = new Client();
        for(Client client : liste) {
          if(client.getId().equals(id)) {
              find  = client;
              break;
          }
        }
        return find;
    }
    
    // Supprimer grâce à l'id
    public void supprimer(Integer id) {
        for(Client client : liste) {
          if(client.getId().equals(id)) {
              liste.remove(client);
              return;
          }
        }
    }
    
    // Supprimer Un client donné
    public void supprimer(Client client) {
        liste.remove(client);
    }
    
    // Lister les éléments
    public List<Client> lister() {
        return liste;
    }
    
    // Lister les éléments à partir de début
    public List<Client> lister(int debut, int nombre) {
        List<Client> element = new LinkedList<>();
        for(int i = debut - 1; i < nombre; i++) {
            element.add(liste.get(i));
        }
        
        return element;
    }
    
}
