/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.mycompany.boutique.Produit;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author toma
 */
public class ProduitService {
    
    static List<Produit> liste;
    
    // ajouter un produit
    public void ajouter(Produit produit) {
        liste.add(produit);
    }
    
    // Modifier un produit dans la liste
    public void modifier(Produit e) {
        for(Produit produit : liste) {
            if(produit.getId().equals(e.getId())) {
                liste.set(liste.indexOf(produit), e);
                return;
            }
        }
    }
    
    // Trouver un produit
    public Produit trouver(Integer id) {
        Produit find = new Produit();
        for(Produit produit : liste) {
            if(produit.getId().equals(id)) {
                find = produit;
                break;
            }
        }
        return find;
    }
    
    // Supprimer un produit à partir de l'id
    public void supprimer(Integer id) {
        for(Produit produit : liste) {
            if(produit.getId().equals(id)) {
                liste.remove(produit);
                return;
            }
        }
    }
    
    // Supprimer un produit donné
    public void supprimer(Produit produit) {
        liste.remove(produit);
    }
    
    // Lister les produits
    public List<Produit> lister() {
        return liste;
    }
    
    // Lister les produit à partir d'un nombre
    public List<Produit> lister(int debut, int nombre) {
        List<Produit> elements = new LinkedList<>();
        for(int i = debut - 1; i < nombre; i++ ) {
            elements.add(liste.get(i));
        }
        
        return elements;
    }
    
    
    
}
