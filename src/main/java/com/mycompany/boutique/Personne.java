/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.boutique;

import java.time.LocalDate;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;

/**
 *
 * @author toma
 */
public class Personne {
    private final static AtomicLong count = new AtomicLong(0);
    protected Long id;
    protected String nom;
    protected String prenom;
    protected LocalDate dateNaissance;
    
    // Constructeur
    public Personne(String nom, String prenom, LocalDate dateNaissance) {
        this.id = count.incrementAndGet();
        this.nom = nom;
        this.prenom = prenom;
        this.dateNaissance = dateNaissance;
    }
    
    
    
    // Les methodes
    public int getAge() {
        return LocalDate.now().getYear() - dateNaissance.getYear();
    }
    
    public int getAge(LocalDate ref) {
        return LocalDate.now().getYear() - ref.getYear();
    }
    
    // Redéfinition des Mehodes
    @Override
    public String toString() {
        return "Nom : "+this.nom+"\nPrenom : "+this.prenom+"\nAge : "+this.getAge()+" ans";
    }
    
    @Override
    public int hashCode() {
        int hash = 1;
        hash = hash * 18 + this.id.hashCode();
        hash = hash * 15 + this.nom.hashCode();
        hash = hash * 8 + this.prenom.hashCode();
        hash = hash * 10 + this.dateNaissance.hashCode();
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Personne other = (Personne) obj;
        if (!Objects.equals(this.nom, other.nom)) {
            return false;
        }
        if (!Objects.equals(this.prenom, other.prenom)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.dateNaissance, other.dateNaissance)) {
            return false;
        }
        return true;
    }
    
    
    
    
    
}
