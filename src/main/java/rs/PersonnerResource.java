/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;

import com.mycompany.boutique.Personne;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import service.PersonneService;

/**
 *
 * @author toma
 */
@Path("/personne")
public class PersonnerResource {
    
    PersonneService personneService;
    
    @POST
    public void ajouter(@QueryParam("personne") Personne personne) {
        personneService.ajouter(personne);
    }
    
    @POST
    public void modifier(@QueryParam("personner") Personne personne) {
        personneService.modifier(personne);
    }
    
    @GET
    public Personne trouver(@QueryParam("id") Integer id) {
        return personneService.trouver(id);
    }
    
    @POST
    public void supprimer(@QueryParam("id") Integer id) {
        personneService.supprimer(id);
    }
    
    @POST
    public void supprimer(@QueryParam("personner") Personne personne) {
        personneService.supprimer(personne);
    }
    
    @GET
    public List<Personne> lister() {
        return personneService.lister();
    }
    
    @GET
    public List<Personne> lister(@QueryParam("debut") int debut, @QueryParam("nombre") int nombre) {
        return personneService.lister(debut, nombre);
    }
    
}
