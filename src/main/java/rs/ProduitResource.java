/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;

import com.mycompany.boutique.Produit;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import service.ProduitService;

/**
 *
 * @author toma
 */
@Path("/produit")
public class ProduitResource {
    
    ProduitService produitService;
    
    @POST
    public void ajouter(@QueryParam("produit") Produit produit) {
        produitService.ajouter(produit);
    }
    
    @POST
    public void modifier(@QueryParam("produit") Produit produit) {
        produitService.modifier(produit);
    }
    
    @GET
    public Produit trouver(@QueryParam("id") Integer id) {
        return produitService.trouver(id);
    }
    
    @POST
    public void supprimer(@QueryParam("id") Integer id) {
        produitService.supprimer(id);
    }
    
    @POST
    public void supprimer(@QueryParam("produit") Produit produit) {
        produitService.supprimer(produit);
    }
    
    @GET
    public List<Produit> lister() {
        return produitService.lister();
    }
    
    @GET 
    public List<Produit>lister(@QueryParam("debut") int debut, @QueryParam("nombre") int nombre) {
        return produitService.lister(debut, nombre);
    }
    
    
}
